# Battleship

Your task is to write the classic battleship game in JavaScript.

Learn about the game and what the rules are here: https://en.wikipedia.org/wiki/Battleship_(game)

Use the provided html and js files as a starting point, or just create your own from scratch: your choice.

---

Requirements:

- Draw a grid that is the game board
- When clicking cells, they should indicate whether it's a hit or a miss
- When all ships are sunk, the game is over

Optional (if you feel that you have the time):

- Write code that will dynamically put the ships on the game board when initializing a new game
- Write game player logic so that you can play against the computer
- Anything else you can think of :-)

We prefer that you deliver your solution using GitHub or similar version control system.