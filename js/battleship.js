const rows = 10;
const cols = 10;
const squareSize = 50;
let hitCount = 0;
let gameBoardElement = document.getElementById("gameboard");

let gameBoard = [
				[0,0,0,0,0,0,0,0,0,0],
				[0,0,0,1,1,1,1,0,1,0],
				[0,0,0,0,0,0,0,0,1,0],
				[0,0,0,0,0,0,1,0,0,0],
				[0,0,0,1,0,0,1,0,0,0],
				[0,0,0,1,0,0,1,0,0,0],
				[0,0,0,1,0,0,0,0,0,0],
				[0,0,0,1,0,0,0,0,0,0],
				[0,0,0,1,0,0,1,1,1,0],
				[0,0,0,0,0,0,0,0,0,0]
				]

function startNewGame() {

		hitCount = 0;
		// console.log(hitCount);
		showMessage('');

		for (col = 0; col < cols; col++) {
				for (row = 0; row < rows; row++) {
					let box = document.createElement("div");
					gameBoardElement.appendChild(box);

					box.id = 'cell' + col + row;

					let topPosition = col * squareSize;
					let leftPosition = row * squareSize;

					box.style.top = topPosition + 'px';
					box.style.left = leftPosition + 'px';
				}
		}
}

gameBoardElement.addEventListener("click", fire, false);

function fire(e) {
		if (e.target !== e.currentTarget) {
				let row = e.target.id.substring(4,5);
				let col = e.target.id.substring(5,6);
				// console.log(row, col);

				if (gameBoard[row][col] == 0){
						// console.log('miss')
						e.target.style.background = 'lightBlue';
				}else if (gameBoard[row][col] == 1) {
						// console.log('hit!')
						e.target.style.background = 'red';

						hitCount++;
						// console.log(hitCount);

						if(hitCount == 17) {
								console.log("You win! All ships have been sunk")
								showMessage('You win! All ships have been sunk');
						}
				}
		}
}

function revealAllCells() {
		// reveal all cells in the game board (when winning or giving up)
		for (col = 0; col < cols; col++) {
				for (row = 0; row < rows; row++) {

						let box = document.createElement("div");
						gameBoardElement.appendChild(box);

						box.id = 'cell' + col + row;

						let topPosition = col * squareSize;
						let leftPosition = row * squareSize;

						box.style.top = topPosition + 'px';
						box.style.left = leftPosition + 'px';

						if (gameBoard[col][row] == 0){
								box.style.background = 'lightBlue';
						}else if (gameBoard[col][row] == 1) {
								box.style.background = 'red';
						}
				}
		}
		showMessage('You gave up! Here are the ships hidden');
}

function showMessage (theMessage) {
		const messageElement = document.getElementById("msg");
		messageElement.textContent = theMessage;
}


startNewGame();
